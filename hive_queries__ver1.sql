-- create database `test_shema`
CREATE DATABASE IF NOT EXISTS test_shema;
-- create database `test_shema2` with explicit location
CREATE DATABASE IF NOT EXISTS test_shema2
LOCATION '/user/ostap_volodya';
-- list all databases
SHOW DATABASES;
-- list all databases which name has '2' in its name
SHOW DATABASES LIKE '*2*';
-- drop database `test_shema2`
DROP DATABASE IF EXISTS test_shema2 CASCADE;


-- (everything below do in scope of `test_shema` database)
USE test_shema;
-- create managed table `tab1e_1` with columns (`id` - type integer, `name` - type string)
CREATE TABLE IF NOT EXISTS tab1e_1(
id int, 
name String) 
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE;
-- insert (1, 'aaa'), (2, 'bbb') into table `tab1e_1`
INSERT INTO tab1e_1
VALUES(1, 'aaa'),(2, 'bbb');
-- show content of the table `tab1e_1`
SELECT * FROM tab1e_1;

-- add column (`type` - type int) into table `tab1e_1` after column `id` and before column `name`
CREATE TABLE IF NOT EXISTS container AS
SELECT * FROM tab1e_1;
DROP TABLE tab1e_1;
ALTER TABLE container ADD COLUMNS (type int);
CREATE TABLE tab1e_1 AS
SELECT id, type, name FROM container;
-- show content of the table `tab1e_1`
SELECT * FROM tab1e_1;

-- append table `tab1e_1` with (3, 2, 'ccc')
INSERT INTO tab1e_1
VALUES(3, 2, 'ccc');
-- show content of the table `tab1e_1`
SELECT * FROM tab1e_1;

-- convert this table `tab1e_1` into bucket table with bucket column `type`
ALTER TABLE tab1e_1 RENAME TO container_4;
CREATE TABLE IF NOT EXISTS tab1e_1(
id int,
type int, 
name String) 
CLUSTERED BY(type) INTO 4 BUCKETS
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE;
INSERT OVERWRITE TABLE tab1e_1
SELECT * FROM container_4;
-- show content of the table `tab1e_1`
SELECT * FROM tab1e_1;

-- via hdfs show the list of all files which are used by `tab1e_1`
SELECT DISTINCT FILE FROM (SELECT *, INPUT__FILE__NAME AS FILE FROM tab1e_1) t;
-- [?in several steps?] make table `tab1e_1` consists of 1 file (in hdfs)
???
-- change table `tab1e_1` into external
ALTER TABLE tab1e_1 SET TBLPROPERTIES('EXTERNAL'='TRUE');

-- show table schema of `tab1e_1` to be sure table is external
DESC FORMATTED tab1e_1;
-- drop table `tab1e_1`
DROP TABLE tab1e_1;

-- create new unmanaged table `tab1e_2` (w/o bucketing) with the content of the previous table `tab1e_1`
CREATE EXTERNAL TABLE IF NOT EXISTS tab1e_2(
id int, 
type int,
name String) 
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE;
LOAD DATA INPATH '/warehouse/tablespace/managed/hive/test_shema.db/tab1e_1' 
INTO TABLE tab1e_2;
-- show content of the table `tab1e_2`
SELECT * FROM tab1e_2;

-- [?in several steps?] change this table into unmanaged partitioned by `type` column
CREATE TABLE IF NOT EXISTS container_2 AS
SELECT * FROM tab1e_2;
DROP TABLE tab1e_2;
CREATE TABLE tab1e_2  (
id int,
name String) 
PARTITIONED BY (type int);
INSERT OVERWRITE TABLE tab1e_2 PARTITION(type) 
SELECT * FROM container_2;
-- add new column (`action_time` - type date) at the latest column
CREATE TABLE IF NOT EXISTS container_3 AS
SELECT * FROM tab1e_2;
DROP TABLE tab1e_2;
ALTER TABLE container_3 ADD COLUMNS (action_time date);
CREATE TABLE tab1e_2 AS
SELECT id, name, action_time, type FROM container_3;
-- show content of the table `tab1e_2`
SELECT * FROM tab1e_2;

-- append table `tab1e_2` with (4, 'ddd', '2018-10-31', 2), (5, 'eee', '2019-11-01', 3)
INSERT INTO tab1e_2
VALUES(4, 'ddd', '2018-10-31', 2), (5, 'eee', '2019-11-01', 3);
-- show content of the table `tab1e_2`
SELECT * FROM tab1e_2;

-- show data ordered by `action_time`
SELECT * FROM tab1e_2 
ORDER BY action_time;

-- show data which `action_time` is greater than today (today is not hardcoded)
SELECT * FROM tab1e_2
WHERE action_time > current_date();

-- show data which `action_time` is in 2019 year
SELECT * FROM tab1e_2 
WHERE action_time > '2018-12-31' and action_time < '2020-01-01';

-- for every `type` show first record only (orderied by `name` column)
SELECT type, first.id, first.name, first.action_time 
FROM (SELECT type, min(named_struct('id',id,'name',name,'action_time',action_time)) AS first 
FROM test_shema.tab1e_2 GROUP BY type) t ORDER BY first.name;
-- do previous step using OVER clause
SELECT type, FIRST_VALUE(id) over (PARTITION BY type) AS first_id, 
FIRST_VALUE(name) over (PARTITION BY type) AS first_name, 
FIRST_VALUE(action_time) over (PARTITION BY type) AS first_time 
FROM test_shema.tab1e_2 ORDER BY first_name;

-- list all partitions of `tab1e_2`
SHOW PARTITIONS test_shema.tab1e_2;
-- drop all partitions except partition '2' ('type=2')
ALTER TABLE tab1e_2 DROP IF EXISTS PARTITION (type != '2');
-- show content of the table `tab1e_2`
SELECT * FROM tab1e_2;

-- create new managed table `tab1e_3` from the select of `table_2`
--        with specific location
--        with compression snappy
--        with type orc
CREATE TABLE IF NOT EXISTS tab1e_3 AS
SELECT * FROM tab1e_2
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
LOCATION '/user/ostap_volodya'
STORED AS ORC tblproperties ('orc.compress' = 'SNAPPY'); 
-- show content of table `tab1e_3`
SELECT * FROM tab1e_3;


-- list all tables in database `test_shema`
SHOW TABLES IN test_shema;
-- list all tables in database `test_shema` which name contains '3'
SHOW TABLES IN test_shema LIKE '*3*';
-- drop test_shema (in 1 command)
DROP DATABASE IF EXISTS test_shema CASCADE;